/*   Copyright 2013 Neil Musgrove

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Winthenticator
{
    public partial class KeyInput : Form
    {
        int exiting = 0;
        public KeyInput()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            object value = Application.UserAppDataRegistry.GetValue("key");
            if (value != null)
            {
                textBox1.Text = value.ToString();
            }
        }

        private void KeyInput_SizeChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
            }

        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(lastcode.ToString());
        }

        private void KeyInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (exiting == 0 && e.CloseReason != CloseReason.WindowsShutDown)
            {
                e.Cancel = true;
                WindowState = FormWindowState.Minimized;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exiting = 1;
            Application.Exit();
        }

        private void modifyKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.UserAppDataRegistry.SetValue("key", textBox1.Text);
            WindowState = FormWindowState.Minimized;
        }

        private string calckey(String baseKey, long ts)
        {
            ts /= 30;
            int base32len = baseKey.Length;
            int secretlen = (base32len * 5 + 7) / 8;
            byte[] deckey = MhanoHarkness.Base32Url.FromBase32String(textBox1.Text);
            for (int i = secretlen; i < deckey.Length; i++)
            {
                deckey[i] = 0;
            }

            long chlg=ts;
            byte[] challenge;
            challenge = new byte[8];

            for (int j=7;j>=0;j--) {
                challenge[j]=(byte)((int)chlg&0xff);
                chlg >>= 8;
            }

            System.Security.Cryptography.HMAC sha1prov = System.Security.Cryptography.HMACSHA1.Create();

            sha1prov.Key = deckey;
            byte[] result = sha1prov.ComputeHash(challenge);

            int offset = result[result.Length - 1] & 0xf;

            int truncatedHash = 0;
            for (int j = 0; j < 4; j++)
            {
                truncatedHash <<= 8;
                truncatedHash |= result[offset + j];
            }

            truncatedHash &= 0x7FFFFFFF;
            truncatedHash %= 1000000;
            string ret = truncatedHash.ToString();
            if (ret.Length < 6)
            {
                return ret.PadLeft(6, '0');
            }
            else
            {
                return ret;
            }
        }

        long lasttime;
        string lastcode;

        private void timer1_Tick(object sender, EventArgs e)
        {
            long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
            ticks /= 10000000; //Convert windows ticks to seconds
            long remainder = ticks % 30;
            System.Resources.ResourceManager rm=Properties.Resources.ResourceManager;
            notifyIcon1.Icon = (Icon)rm.GetObject("ga"+(remainder+1));
            remainder = 30 - remainder;
            if (lasttime != ticks)
            {
                lastcode = calckey(textBox1.Text, ticks);
                lasttime = ticks;
            }
            notifyIcon1.Text=lastcode.ToString()+" ("+remainder.ToString()+")";
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lastcode.ToString());
        }
    }
}